import socket
import sys
import random
import time

s = socket.socket()
s.bind(('', 85))
s.listen(5)

address = '127.0.0.1'
xml_payload = '<?xml version="1.0" encoding="UTF-8"?>\n<Actions>\n\t<Action Target="RandomSoft" Action="Ping" Location="Default" Timer="0"/>\n</Actions>\n'
expected_response = '<?xml version="1.0" encoding="UTF-8"?>\n<Actions>\n\t<Action Target="RandomSoft" Action="Ping" Location="Default" Timer="0"/>\n\t<Response>Ok</Response></Actions>\n'
expected_fail = '<?xml version="1.0" encoding="UTF-8"?>\n<Actions>\n\t<Action Target="RandomSoft" Action="Ping" Location="Default" Timer="0"/>\n\t<Response>Ko</Response></Actions>\n'

responses = [expected_response, expected_fail, 'timeout']
bsp_stop = '<rc version=\"1\" id=\"1\" action=\"stop\" frame_id=\"0\" />\r\n\r\n'
bsp_port = 2324

while True:
	print('Listening')
	(clientsocket, address) = s.accept()
	try:
		msg = clientsocket.recv(2048)
		print('Received msg: %s' % msg)
		choice = random.choice(responses)
		if choice != 'timeout':
			print('Sending msg: %s' % bytes(choice))
			clientsocket.send(bytes(choice))
			if choice == expected_response:
				sock = socket.socket()
				if sock:
					sock.connect(('127.0.0.1', bsp_port))
					print('Sending bsp stop msg')
					sock.send(bytes(bsp_stop))
					sock.close()
		else:
			print('timing out')
			time.sleep(3)
	except socket.error:
		if socket.error.errno == 104:
		    print('Connection reset. Listening for a new one')
        

