import _winreg
import socket
import os
import sys
import time
import ConfigParser


class BSPBundle:

    def __init__(self, sock=None):
        if sock is None:
            self.sock = socket.socket()
        else:
            self.sock = sock
        self.config = os.path.join(os.getcwd(), 'config.ini')
        if os.path.isfile(self.config):
            self.read_conf()
        else:
            print 'No config file provided'
            sys.exit()

    def read_conf(self):
        config = ConfigParser.ConfigParser()
        if config.read(self.config):
            self.payload = config.get('MESSAGES', 'bsp_msg')
            self.address = config.get('ENV', 'server_address') or '127.0.0.1'
            self.expected_response = config.get('MESSAGES', 'ok_msg')
            self.expected_fail = config.get('MESSAGES', 'ko_msg')
            self.response_timeout = int(config.get('ENV', 'response_timeout') or os.environ.get('response_timeout', 1000)) # timeout in ms, if not in config - env_var will be used - if not set -> default of 1000 ms
            self.log = config.get('ENV', 'log') or os.environ.get('log')
            self.port = int(config.get('ENV', 'port'))
            return True
        else:
            print 'Cannot read config file: %s' % self.config
            sys.exit() 


    def write_log(self, msg):
        if self.log:
            cur_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
            try:
                l = open(self.log, 'a')
                l.write('[%s] (viadirect) %s \n' % (cur_time, msg))
                l.close()
            except IOError:
                pass
        else:
            return

    def connect(self):
        if self.sock and self.address and self.port:
            server_address = (self.address, self.port)
            self.sock.settimeout(float(self.response_timeout) / 1000)
            try:
                self.sock.connect(server_address)
                return True
            except socket.error:
                if socket.error.errno == 111:
                    self.write_log('Connection refused')
                    sys.exit()
            except socket.timeout:
                self.write_log('Connection timed out')
                sys.exit()
        else:
            return False

    def receive_all(self):
        self.sock.settimeout(float(self.response_timeout) / 1000)
        try:
            response = self.sock.recv(2048)
        except socket.timeout:
            self.write_log('Response timed out')
            sys.exit()
        except socket.error:
            if socket.error.errno == 104:
                self.write_log('Connection reset when receving request')
                sys.exit()
        try:
           self.sock.close()
        except OSError:
           pass
        finally:
           return response


    def bs_query(self):
        raw_xml = bytes(self.payload)
        if self.connect():
            try:
                self.sock.send(raw_xml)
                raw_response = str(self.receive_all())
                if raw_response.encode('string_escape') == self.expected_response:
                    log_msg = 'Received OK response: ' + raw_response.encode('string_escape')
                    self.write_log(log_msg)
                    while True:
                        time.sleep(10)
                else:
                    log_msg = 'Received KO response: ' + raw_response.encode('string_escape')
                    self.write_log(log_msg)
                    sys.exit()
            except socket.error:
                if socket.error.errno == 104:
                    self.write_log('Connection reset when sending query')
                    sys.exit()
        else:
            self.write_log('Connection refused')
            sys.exit()
    

if __name__ == "__main__":
    bs = BSPBundle()
    bs.bs_query()

