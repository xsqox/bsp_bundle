# BSP_BUNDLE

## Script setup per VIADIRECT specifications:

1. BSPlayer and Application should be running simultaneously. Application should be running on the background so that it can listen to incoming connections (port can be specified in config.ini, the default is 85)

2. Script should be set up as a triggered Ad Copy, and will run every time BSP receives the trigger (for VIADirect an interactivity trigger is sent when the user touches the screen).

3. When triggered, script will send payload message to the port where Application is listening. (Payload message must be specified in config.ini as bsp_msg. Your application must be aware of that message)

4. After sending the message, script will wait for response (waiting timeout can be specified in config.ini as response_timeout or default 1000ms will be used)

5. When receiving the message from the script, the application should promote itself to being "on top" and reply with the configured "OK" response (must be specified in config.ini as ok_msg).

6. If ok response is received, script will go to sleep and BSPlayer will await STOP message to come from the Application to default port 2324.

BSPlayer stop message:
'<rc version=\"1\" id=\"1\" action=\"stop\" frame_id=\"0\" />\r\n\r\n'


7. If ko response is received (must be specified in config.ini as ko_msg), script will be killed and BSP will resume playback of its loop. 

8. If no response is received within specified or default timeout, script will be killed and BSP will resume playback of its loop.


## Setup requirements:
1. Python 2.7
2. py2exe - http://sourceforge.net/projects/py2exe/files/py2exe/0.6.9/py2exe-0.6.9.win32-py2.7.exe/download

To build bsp_bundle.exe from bsp_bundle.py
1. cd to the directory with bsp_bundle.py
2. C:\Python27\python.exe setup.py py2exe

OR run from directory with bsp_bundle.py:
build_binary.sh - LINUX
build_binary.bat - Windows

Configuration file:
1. must be called config.ini
2. must be placed into the zip archive containing bsp_bundle.exe

bsp_bundle_server emulator:
1. Basic example of server communicating with bsp
2. Make sure to update example msgs to match your config